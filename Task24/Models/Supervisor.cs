﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;
using Xunit.Sdk;
using System.ComponentModel.DataAnnotations;

namespace Task24.Models
{
    public class Supervisor
    {
        [Required(ErrorMessage = "Please enter a valid id number")]
        [RegularExpression("^\\d+$", ErrorMessage = "Please enter a valid whole number")]
        public int Id { get; set; }

        [Required(ErrorMessage = "Please enter a name")]
        public string Name { get; set; }


        public override string ToString()
        {
            return Name;
        }
    }
}
