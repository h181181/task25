﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Task24.Models;

namespace Task24.Controllers
{
    public class HomeController : Controller
    {

        public IActionResult Index()
        {
            SupervisorGroup.AddSupervisor(new Supervisor { Id = 1, Name="John"});
            SupervisorGroup.AddSupervisor(new Supervisor { Id = 2, Name="Carla"});
            SupervisorGroup.AddSupervisor(new Supervisor { Id = 3, Name="Sandra"});
            SupervisorGroup.AddSupervisor(new Supervisor { Id = 4, Name = "Anders" });
           
            ViewBag.timeStamp = DateTime.Now;
            return View("MyFirstView");
            
        }

        [HttpGet]
        public IActionResult SupervisorInfo()
        {
            return View();
        }

        [HttpPost]
        public IActionResult SupervisorInfo(Supervisor supervisor)
        {
            if (ModelState.IsValid)
            {
                SupervisorGroup.Supervisors.Add(supervisor);

                return View("AddSupervisorConfirmation", supervisor);
            }
            else
            {
                return View("MyFirstView");
            }
        }
      

        [HttpGet]
        public IActionResult AllSupervisors(Supervisor supervisor)
        {
            return View(SupervisorGroup.Supervisors);
        }

    }
}
